﻿function addItemToBasket(id)
{
    $.ajax({
        type: 'post',
        url: '/api/basket/' + id,
        dataType: 'json',        
        success: function (data)
        {
            $('#basketCount').html(data.totalItems);
            $('#basketPrice').html(data.totalPrice);
        }
    }

    )
}

function deleteItemFromBasketPage(id)
{
    
    $.ajax({
        type: 'delete',
        url: '/api/basket/' + id,
        dataType: 'json',
        success: function (data) {
            if (data === undefined)
            {
                location.reload();
            }
            $('#basketCount').html(data.totalItems);
            $('#basketPrice').html(data.totalPrice);
            $('#basketPricePage').html(data.totalPrice);



            
            saveChangesForItem(data.items, id);

        }
    }

    )
}

function addItemToBasketPage(id) {
    $.ajax({
        type: 'post',
        url: '/api/basket/' + id,
        dataType: 'json',
        success: function (data) {
            $('#basketCount').html(data.totalItems);
            $('#basketPrice').html(data.totalPrice);
            saveChangesForItem(data.items, id);
            $('#basketPricePage').html(data.totalPrice);

            
            
        }
    }

    )
}

function saveChangesForItem(items, id)
{
    var item;
    for (var i = 0; i < items.length; i++) {
        if (items[i].value.id === id ) {
            item = items[i];
        }
    }
    if (item === undefined)
    {
        location.reload();
    }
    var l1 = ('.basket__items .basket__nums input[name=') + (id) + ']';
    var l2 = ('.basket__items .basket__tot strong[id=') + (id) + ']';
    $(l1).val(item.count);
    $(l2).html(item.count * item.value.price);
    }


function deleteFullItem(id)
{
    $.ajax({
        type: 'delete',
        url: '/api/basket/' + id+'/full',
        dataType: 'json',
        success: function () {
            
        location.reload();
           
        }
    }

    )
}


