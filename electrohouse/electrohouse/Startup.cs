﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(electrohouse.Startup))]
namespace electrohouse
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
