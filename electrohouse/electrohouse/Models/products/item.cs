﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace electrohouse.Models.products
{
    public class item
    {
        public virtual int ID { get; set; }
        public virtual string name { get; set; }
        public virtual double price { get; set; }
        public virtual double priceOld { get; set; }

        public virtual Category Category { get; set; }
        public virtual string CategoryName { get; set; }
        public virtual bool isNew { get; set; }
        public virtual bool isStock { get; set; }
        public virtual bool isPopular { get; set; }
        public virtual string path { get; set; }

        public virtual string alterPath1 { get; set; }
        public virtual string alterPath2 { get; set; }

        public virtual string alterPath3 { get; set; }



        public virtual brand brand { get; set; }

        
        public virtual int likes { get; set; }

        
        public virtual bool available { get; set; }

        public virtual List<string> characteristicsShort { get; set; }
        
        public virtual List<Comment> comments { get; set; }
        public virtual List<deailCharacteristicSection> characteristics { get; set; }

        public int guarantee { get; set; }

        public string description { get; set; }


        public item()
        {

        }
        public item(string _name, double _price, Category _category, brand _brand, bool _isNew, bool _isStock, bool _isPopular, string _path, int _rating, int _likes)
        {
            name = _name;
            price = _price;
            Category = _category;
            brand = _brand;
            isNew = _isNew;
            isPopular = _isPopular;
            isStock = _isStock;
            path = _path;            
            likes = _likes;

        }



    }

    public class Comment
    {
        public int ID { get; set; }
        public string userName { get; set; }
        public string comment { get; set; }
        public DateTime date { get; set; }

        public Comment()
        {

        }
    }

    public class deailCharacteristicSection
    {
        public virtual int ID { get; set; }
        public virtual String Caption { get; set; }
        public virtual List<CharacteristicString> characteristics { get; set; }
        public deailCharacteristicSection()
        {

        }
    }


    public enum Category
    {
        phone,
        ipad,
        computer,
        audio,
        television,
        washing,
        electro,
        photo,
        garden,
        reparation,
        auto,
        router
    }

   
}

public class CharacteristicString
{
    public virtual int ID { get; set; }
    public virtual string characteristic { get; set; }
    public virtual bool isShort { get; set; }

    public CharacteristicString()
    {

    }
}