﻿using electrohouse.Models.products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace electrohouse.Models.products
{
    public class Order : IDisposable
    {

        public int ID { get; set; }
        public List<Pair<int, item>> items = new List<Pair<int, item>>();
        public string userId { get; set; }
        public OrderState state { get; set; }

        public DateTime RegistrationDate { get; set; }

        public int totalItems { get; set; }
        public double totalPrice { get; set; }

        /*
        public string nameOfCustomer { get; set; }
        public string email {get;set;}
        public string phone {get;set;}
        public string town {get;set;}   
        
        public string deliveryType { get; set; }
        public string paymentType { get; set; }
        */


        public Order()
        {

        }

        public bool hasItem(int id)
        {
            if (items == null)
                return false;
            
            return (items.Where(x => x.value.ID == id).FirstOrDefault() != null) ? true : false;
        }

        public item getItemById(int id)
        {
            if (hasItem(id))
            {
                return items.FirstOrDefault(x => x.value.ID == id).value;
            }

            return null;
        }



        public void AddItem(int id)
        {
            if (hasItem(id))
            {
                items.First(x => x.value.ID == id).count++;
            }
            else
            {
                var item = db.Items.FirstOrDefault(x => x.ID == id);
                if (item != null)
                    items.Add(new Pair<int, item> { count = 1, value = item });
            }

            recountBasket();
        }

        public void DeleteOneItem(int id)
        {
            if (hasItem(id))
            {

                if (items.First(x => x.value.ID == id).count != 0)
                {
                    items.First(x => x.value.ID == id).count--;
                }
                
            }
            recountBasket();
        }

        public void DeleteItem(int id)
        {
            items.RemoveAll(x => x.value.ID == id);
            recountBasket();
        }

        public void Dispose()
        {
            db.Dispose();
        }

        public void countItems()
        {
            totalItems = 0;
            foreach (var item in items)
            {
                totalItems += item.count;
            }
        }

        public void countPrice()
        {
            totalPrice = 0;
            foreach (var item in items)
            {
                totalPrice += item.value.price * item.count;
            }
        }

        public void recountBasket()
        {
            countPrice();
            countItems();
        }
               

        private ApplicationDbContext db = new ApplicationDbContext();
    }


    public enum OrderState
    {
        inPrepare,
        Processing,
        Complete,
        Done
    }

    public class Pair<T1, T2>
    {
        public T1 count { get; set; }
        public T2 value { get; set; }
    }
}
