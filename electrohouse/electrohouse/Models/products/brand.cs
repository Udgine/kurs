﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace electrohouse.Models.products
{
    public class brand
    {
        public virtual int ID { get; set; }
        public virtual string name { get; set; }
        

        public brand()
        {

        }
        public brand(string _name)
        {
            name = _name;
            
        }

        public override string ToString()
        {
            return name;
        }
    }
}