﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace electrohouse.Models
{
    public class Asistance
    {
        public string Name { get; set; }
        public bool Checked { get; set; }
    }

    public class FilterItemConf
    {
        public List<string> brands = new List<string>();
        public int price1 { get; set; }
        public int price2 { get; set; }

        public bool available { get; set; }

        public bool isOn { get; set; }

    }
}