﻿using electrohouse.Models.products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace electrohouse.Models
{
    public class Question
    {
        public int ID { get; set; }
        public string question { get; set; }

        public string answer { get; set; }

        public Question()
        {

        }

        public Question(string _question, string _answer)
        {
            question = _question;
            answer = _answer;
        }

    }
}