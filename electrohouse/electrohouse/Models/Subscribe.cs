﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace electrohouse.Models
{
    public class Subscribe
    {
        public int ID { get; set; }

        [DataType(DataType.EmailAddress)]
        public string email { get; set; }

        public Subscribe()
        {

        }
        public Subscribe(string _email)
        {
            email = _email;
        }
    }
}