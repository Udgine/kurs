﻿using electrohouse.Models.products;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace electrohouse.Models
{
    public class Feedback
    {
        public int ID { get; set; }

        [DataType(DataType.EmailAddress)]
        public string email { get; set; }
        public string question { get; set; }
        public bool isSend { get; set; }

        public Feedback()
        {

        }
        public Feedback(string _email, string _question)
        {
            question = _question;
            email = _email;
        }
    }
}