﻿using electrohouse.Models;
using electrohouse.Models.products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace electrohouse.Controllers
{
    public class CommentController : ApiController, IDisposable
    {
        private ApplicationDbContext db = new ApplicationDbContext();


        // POST: api/Comment
        public Comment Post([FromUri]string id,[FromBody]dynamic values)
        {
            var httpContext = (HttpContextWrapper)Request.Properties["MS_HttpContext"];
            var newComment = httpContext.Request.Form.GetValues("comment").FirstOrDefault();

            var dateOfComment = DateTime.Now;
            string user = User.Identity.Name;

            Comment Comment = new Comment() { userName = user, comment = newComment, date = dateOfComment };
            db.Items.Where(x => x.ID.ToString() == id).FirstOrDefault().comments.Add(Comment);
            db.SaveChanges();


            return Comment;
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
