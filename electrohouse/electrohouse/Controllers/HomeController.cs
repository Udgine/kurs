﻿using electrohouse.Models;

using electrohouse.Models.products;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace electrohouse.Controllers
{

   
    [ValidateInput(false)]
    public class HomeController : Controller, IDisposable
    {
        
        private ApplicationDbContext db = new ApplicationDbContext();
        private ApplicationUserManager _userManager;



        public ActionResult Index()
        {
            ViewBag.Title = "Главная";
            ViewBag.hasSlider = true;
            ViewBag.NewItems = db.Items.Where(x => x.isNew == true).ToList();
            ViewBag.PopularItems = db.Items.Where(x => x.isPopular == true).ToList();
            return View();
        }

        public ActionResult Catalog()
        {
            ViewBag.Title = "Каталог товаров";
            return View();
        }
        


        public ActionResult Category(string q,int? page, string sort)
        {
            List<item> items = new List<item>();
            FilterItemConf filter = new FilterItemConf();
            if (page == null)
            {
                Session["itemsConfig"] = null;
            }
            page = (page != null) ? page : 1;



            switch (q)
            {
                case "Stock":
                    ViewBag.Title = "Акции";
                    items = db.Items.Where(x => x.isStock == true).ToList();
                    break;
                case "New":
                    ViewBag.Title = "Новые поступления";
                    items = db.Items.Where(x => x.isNew == true).ToList();
                    break;
                case "Popular":
                    ViewBag.Title = "Популярное";
                    items = db.Items.Where(x => x.isPopular == true).ToList();
                    break;
                case "phone":
                    ViewBag.Title = "Мобильные телефоны";
                    items = db.Items.Where(x => x.Category == Models.products.Category.phone ).ToList();                    
                    break;
                case "ipad":
                    ViewBag.Title = "Планшеты";
                    items = db.Items.Where(x => x.Category == Models.products.Category.ipad).ToList();

                    break;
                case "computer":
                    ViewBag.Title = "Копмьютеры";
                    items = db.Items.Where(x => x.Category == Models.products.Category.computer).ToList();

                    break;
                case "audio":
                    ViewBag.Title = "Аудиосистемы";
                    items = db.Items.Where(x => x.Category == Models.products.Category.audio).ToList();

                    break;
                case "television":
                    ViewBag.Title = "Телевизоры";
                    items = db.Items.Where(x => x.Category == Models.products.Category.television).ToList();

                    break;
                case "washing":
                    ViewBag.Title = "Стиральные машины";
                    items = db.Items.Where(x => x.Category == Models.products.Category.washing).ToList();

                    break;
                case "electro":
                    ViewBag.Title = "Электроприборы";
                    items = db.Items.Where(x => x.Category == Models.products.Category.electro).ToList();

                    break;
                case "photo":
                    ViewBag.Title = "Фотоаппараты";
                    items = db.Items.Where(x => x.Category == Models.products.Category.photo).ToList();

                    break;
                case "garden":
                    ViewBag.Title = "Садовые принадлежности";
                    items = db.Items.Where(x => x.Category == Models.products.Category.garden).ToList();

                    break;
                case "reparation":
                    ViewBag.Title = "ХЗ что писать";
                    items = db.Items.Where(x => x.Category == Models.products.Category.reparation).ToList();

                    break;
                case "auto":
                    ViewBag.Title = "ХЗ что писать";
                    items = db.Items.Where(x => x.Category == Models.products.Category.auto).ToList();

                    break;
                case "router":
                    ViewBag.Title = "Роутеры";
                    items = db.Items.Where(x => x.Category == Models.products.Category.router).ToList();
                    break;                                    
                default:
                    
                    break;
            }

            if (Request.Form.Count != 0)
            {
                    foreach (var key in Request.Form.Keys)
                    {
                        filter.brands.Add(key.ToString());
                    }
                    int priceIndex = filter.brands.Count - 2;
                    var tmp = Request.Form.Get(priceIndex).ToString().Split(' ', '$');

                    filter.price1 = int.Parse(tmp[1]);
                    filter.price2 = int.Parse(tmp[4]);

                    filter.available = bool.Parse(Request.Form.Get(priceIndex + 1).ToString());

                    filter.brands.RemoveRange(priceIndex, 2);
                    filter.isOn = true;

                    Session["itemsConfig"] = filter;
             }
            
            

            if (Session["itemsConfig"] != null)
            {
                filter = (FilterItemConf)Session["itemsConfig"];

                if (filter.brands.Count != 0)
                {
                    items = items.Where(x => filter.brands.Contains(x.brand.name)).ToList();
                }
                //var tmp = filter.brands.Contains(items.Select(x => x.brand));

                items = items.Where(x =>    x.price >= filter.price1 &&
                                            x.price <= filter.price2 &&
                                            (filter.available) ? x.available : true
                                   ).ToList();
            }



            ViewBag.sort = sort;
            switch (sort)
            {
                case "rating":
                    items = items.OrderBy(x => x.likes).ToList();
                    break;
                case "upprice":
                    items = items.OrderBy(x => x.price).ToList();
                    break;
                case "downprice":
                    items = items.OrderByDescending(x => x.price).ToList();
                    break;
                case "comments":
                    items = items.OrderBy(x => x.comments.Count).ToList();
                    break;
                default:
                    break;

            }
            List<bool> isNew = new List<bool>();
            List<bool> isPopular = new List<bool>();
            List<bool> isStock = new List<bool>();

            foreach (var item in items)
            {
                isPopular.Add((item.isPopular) ? true : false);
                isStock.Add((item.isStock) ? true : false);
                isNew.Add((item.isNew) ? true : false);
            }


            ViewBag.brands = getBrands(items);





            int maxItemsOnPage = 16;
            ViewBag.page = page;
            ViewBag.q = q;
            ViewBag.pagesCount = (items.Count % maxItemsOnPage == 0) ? (items.Count / maxItemsOnPage) : (items.Count / maxItemsOnPage + 1);
            ViewBag.itemsOnPage = (ViewBag.page == ViewBag.pagesCount) ? (items.Count % maxItemsOnPage) : (maxItemsOnPage);
            ViewBag.start = (ViewBag.page - 1) * maxItemsOnPage;
            ViewBag.end = ViewBag.start + ViewBag.itemsOnPage;

            ViewBag.pageStart = (page - 3 > 0) ? true : false;
            ViewBag.pageEnd = (page + 2 < ViewBag.pagesCount) ?  true : false;

            return View(items);
        }

       
        public ActionResult Search(string q, int? page, string sort)
        {
            
          var items = db.Items.Where(x => x.name.ToLower().Contains(q.ToLower())).ToList();
            if (items.Count == 0) { return View(); } 
          

            List<bool> isNew = new List<bool>();
            List<bool> isPopular = new List<bool>();
            List<bool> isStock = new List<bool>();

            foreach (var item in items)
            {
                isPopular.Add((item.isPopular) ? true : false);
                isStock.Add((item.isStock) ? true : false);
                isNew.Add((item.isNew) ? true : false);
            }

            ViewBag.isPopular = isPopular;
            ViewBag.isStock = isStock;
            ViewBag.isNew = isNew;

            ViewBag.sort = sort;
            switch (sort)
            {
                case "rating":
                    items = items.OrderBy(x => x.likes).ToList();
                    break;
                case "upprice":
                    items = items.OrderBy(x => x.price).ToList();
                    break;
                case "downprice":
                    items = items.OrderByDescending(x => x.price).ToList();
                    break;
                case "comments":
                    items = items.OrderBy(x => x.comments.Count).ToList();
                    break;
                default:
                    break;

            }

            page = (page != null) ? page : 1;
          int maxItemsOnPage = 16;
          ViewBag.page = page;
          ViewBag.q = q;
         ViewBag.pagesCount = (items.Count % maxItemsOnPage == 0) ? (items.Count / maxItemsOnPage) : (items.Count / maxItemsOnPage + 1);
          ViewBag.itemsOnPage = (ViewBag.page == ViewBag.pagesCount) ? (items.Count % maxItemsOnPage) : (maxItemsOnPage);
          ViewBag.start = (ViewBag.page - 1) * maxItemsOnPage;
          ViewBag.end = ViewBag.start + ViewBag.itemsOnPage;


            ViewBag.pageStart = (page - 3 > 0) ? true : false;
            ViewBag.pageEnd = (page + 2 < ViewBag.pagesCount) ? true : false;
            
            if (ViewBag.items == null)
            {
                ViewBag.NotFound = true;
            }

            ViewBag.flag = true;
            ViewBag.integ = 1;
            return View(items);
        }


        public ActionResult Basket()
        {
            Order basket = (Session["basket"] != null) ? (Order)Session["basket"] : null;

            return View(basket);
        }

        public ActionResult Feedback() {return View();}
        public ActionResult Answers() { return View();}
        public ActionResult Guaratee() { return View();}

        public ActionResult Shipping() { return View();}

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            
            return View();
        }

        public ActionResult Item(int q)
        {
           
            var item = db.Items.FirstOrDefault(x => x.ID == q);


            return View(item);
        }

        public ActionResult Checkout()
        {
            if (Session["basket"] == null)
            {
                return RedirectToAction("Basket");
            }

            string phone = "", email = "";
            if (User.Identity.IsAuthenticated)
            { 
            var userId = User.Identity.GetUserId();
            phone = UserManager.GetPhoneNumber(userId);
            email =  UserManager.GetEmail(userId);
            }

            ViewBag.phone = phone;
            ViewBag.email = email;

            return View();
        }

        [HttpPost]
        public ActionResult makeOrder()
        {

            var form = Request.Form;

            return null;
        }

        public List<brand> getBrands(List<item> items)
        {
            var brands = new List<brand>();
            foreach (var item in items)
            {
                if (!brands.Contains(item.brand))
                {
                    brands.Add(item.brand);
                }
            }
            return brands;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}