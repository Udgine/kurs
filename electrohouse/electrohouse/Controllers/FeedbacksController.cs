﻿using electrohouse.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace electrohouse.Controllers
{
    public class FeedbacksController : ApiController, IDisposable
    {

        private ApplicationDbContext db = new ApplicationDbContext();



        // POST: api/Feedbacks
        public void Post([FromBody]dynamic values)
        {
            var httpContext = (HttpContextWrapper)Request.Properties["MS_HttpContext"];
            var email = httpContext.Request.Form.GetValues("email").FirstOrDefault();
            var question = httpContext.Request.Form.GetValues("question").FirstOrDefault();

            db.Feedbacks.Add(new Feedback(email, question));
            db.SaveChanges();
        }




        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
