﻿using electrohouse.Models;
using electrohouse.Models.products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;

namespace electrohouse.Controllers
{
    public class basketController : ApiController, IDisposable
    {
        private HttpSessionState session = HttpContext.Current.Session;
        private ApplicationDbContext db = new ApplicationDbContext();
        // GET: api/basket
        public Order GetBasket()
        {

            return (session["basket"] != null) ? (Order)session["basket"] : null;
        }

        // GET: api/basket/5
        public item GetItem(int id)
        {
            Order basket = (session["basket"] != null) ? (Order)session["basket"] : null;

            return (basket != null) ? basket.getItemById(id) : null;
        }

        // POST: api/basket
        [HttpPost]
        public Order addItem([FromUri]int id)
        {
            Order basket = (session["basket"] != null) ? (Order)session["basket"] : null;
            if (basket == null)
            {
                basket = new Order();

            }

            basket.AddItem(id);

            session["basket"] = basket;

            return basket;
        }

        // PUT: api/basket/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/basket/5
        [HttpDelete]
        public Order deleteItem(int id)
        {
            Order basket = (session["basket"] != null) ? (Order)session["basket"] : null;
            if (basket != null)
            {
                basket.DeleteOneItem(id);
                session["basket"] = (basket.items.Count == 0) ? null : basket;
                return basket;
            }


            return (basket.items.Count == 0) ? null : basket;
        }

        [HttpDelete]
        public void deleteItem(int id, string mode)
        {
            
            Order basket = (session["basket"] != null) ? (Order)session["basket"] : null;
            if (basket != null)
            {
                basket.DeleteItem(id);
                session["basket"] = (basket.items.Count == 0) ? null : basket;
                
            }


           
        }
        

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
