﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using electrohouse.Models;

namespace electrohouse.Controllers.admin
{
    public class CharacteristicStringsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: CharacteristicStrings
        public ActionResult Index(int? id)
        {
            if (id != null)
            {
                var characteristics = db.Characteristics.FirstOrDefault(x => x.ID == id).characteristics.ToList();
                return View(characteristics);
            }
            return View(db.CharacteristicStrings.ToList());
        }

        // GET: CharacteristicStrings/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CharacteristicString characteristicString = db.CharacteristicStrings.Find(id);
            if (characteristicString == null)
            {
                return HttpNotFound();
            }
            return View(characteristicString);
        }

        // GET: CharacteristicStrings/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CharacteristicStrings/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,characteristic,isShort")] CharacteristicString characteristicString)
        {
            if (ModelState.IsValid)
            {
                db.CharacteristicStrings.Add(characteristicString);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(characteristicString);
        }

        // GET: CharacteristicStrings/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CharacteristicString characteristicString = db.CharacteristicStrings.Find(id);
            if (characteristicString == null)
            {
                return HttpNotFound();
            }
            return View(characteristicString);
        }

        // POST: CharacteristicStrings/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,characteristic,isShort")] CharacteristicString characteristicString)
        {
            if (ModelState.IsValid)
            {
                db.Entry(characteristicString).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(characteristicString);
        }

        // GET: CharacteristicStrings/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CharacteristicString characteristicString = db.CharacteristicStrings.Find(id);
            if (characteristicString == null)
            {
                return HttpNotFound();
            }
            return View(characteristicString);
        }

        // POST: CharacteristicStrings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CharacteristicString characteristicString = db.CharacteristicStrings.Find(id);
            db.CharacteristicStrings.Remove(characteristicString);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
