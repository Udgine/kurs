﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using electrohouse.Models;

namespace electrohouse.Controllers
{
    public class QController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Questions1
        public ActionResult Index()
        {
            if (User.Identity.Name != "admin" && User.Identity.Name != "Alexi") { return Redirect(Url.Action("Index", "Home")); }

            return View(db.Questions.ToList());
        }

        // GET: Questions1/Details/5
        public ActionResult Details(int? id)
        {
            if (User.Identity.Name != "admin" && User.Identity.Name != "Alexi") { return Redirect(Url.Action("Index", "Home")); }

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Question question = db.Questions.Find(id);
            if (question == null)
            {
                return HttpNotFound();
            }
            return View(question);
        }

        // GET: Questions1/Create
        public ActionResult Create()
        {
            if (User.Identity.Name != "admin" && User.Identity.Name != "Alexi") { return Redirect(Url.Action("Index", "Home")); }

            return View();
        }

        // POST: Questions1/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,question,answer")] Question question)
        {
            if (User.Identity.Name != "admin" && User.Identity.Name != "Alexi") { return Redirect(Url.Action("Index", "Home")); }

            if (ModelState.IsValid)
            {
                db.Questions.Add(question);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(question);
        }

        // GET: Questions1/Edit/5
        public ActionResult Edit(int? id)
        {
            if (User.Identity.Name != "admin" && User.Identity.Name != "Alexi") { return Redirect(Url.Action("Index", "Home")); }

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Question question = db.Questions.Find(id);
            if (question == null)
            {
                return HttpNotFound();
            }
            return View(question);
        }

        // POST: Questions1/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,question,answer")] Question question)
        {
            if (User.Identity.Name != "admin" && User.Identity.Name != "Alexi") { return Redirect(Url.Action("Index", "Home")); }

            if (ModelState.IsValid)
            {
                db.Entry(question).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(question);
        }

        // GET: Questions1/Delete/5
        public ActionResult Delete(int? id)
        {
            if (User.Identity.Name != "admin" && User.Identity.Name != "Alexi") { return Redirect(Url.Action("Index", "Home")); }

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Question question = db.Questions.Find(id);
            if (question == null)
            {
                return HttpNotFound();
            }
            return View(question);
        }

        // POST: Questions1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (User.Identity.Name != "admin" && User.Identity.Name != "Alexi") { return Redirect(Url.Action("Index", "Home")); }

            Question question = db.Questions.Find(id);
            db.Questions.Remove(question);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
