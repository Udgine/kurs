﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using electrohouse.Models;
using electrohouse.Models.products;

namespace electrohouse.Controllers.admin
{
    public class itemsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: items
        public ActionResult Index()
        {
            return View(db.Items.ToList());
        }

        // GET: items/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            item item = db.Items.Find(id);
            if (item == null)
            {
                return HttpNotFound();
            }
            return View(item);
        }

        // GET: items/Create
        public ActionResult Create()
        {
            ViewBag.brands = db.Brands.Select(x => new SelectListItem()
            {
                Text = x.name
            });
            return View();
        }

        // POST: items/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,name,price,priceOld,Category,CategoryName,isNew,isStock,isPopular,path,alterPath1,alterPath2,alterPath3,likes,available,guarantee,description")] item item, string Brands)
        {
            if (ModelState.IsValid)
            {
                item.brand = db.Brands.FirstOrDefault(x => x.name == Brands);
                db.Items.Add(item);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(item);
        }

        // GET: items/Edit/5
        public ActionResult Edit(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            item item = db.Items.Find(id);
            if (item == null)
            {
                return HttpNotFound();
            }
            ViewBag.brands = db.Brands.ToList();
            return View(item);
        }

        // POST: items/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,name,price,priceOld,Category,CategoryName,isNew,isStock,isPopular,path,alterPath1,alterPath2,alterPath3,likes,available,guarantee,description")] item item, string Brands)
        {
            if (ModelState.IsValid)
            {

                db.Entry(item).State = EntityState.Modified;
                db.SaveChanges();

                /*
                //Change Brand
                var tmp = db.Entry(item).Reference(e => e.brand).EntityEntry;
                var itemToUpdate = db.Items.Find(item.ID);
                //item.brand = db.Brands.FirstOrDefault(x => x.name == Brands);
                


                db.Items.Attach(itemToUpdate);
                var entry = db.Entry(itemToUpdate);
                entry.Reference(e => e.brand).CurrentValue = item.brand;
                
                

                db.SaveChanges();
                */
                return RedirectToAction("Index");
            }
            return View(item);
        }

        /*
         * 
         * 
         * 
         var courseToUpdate = db.Courses.Find(id);
    if (TryUpdateModel(courseToUpdate, "",
       new string[] { "Title", "Credits", "DepartmentID" }))
    {
        try
        {
            db.SaveChanges();

            return RedirectToAction("Index");
        }
         payment = con.Payments.First(x => x.Id == orderId);
        payment.Status = true;

        con.Payments.Attach(payment);
        var entry = con.Entry(payment);
    entry.Property(e => e.Status).IsModified = true;
    con.SaveChanges();
         */

        // GET: items/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            item item = db.Items.Find(id);
            if (item == null)
            {
                return HttpNotFound();
            }
            return View(item);
        }

        // POST: items/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            item item = db.Items.Find(id);
            db.Items.Remove(item);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
