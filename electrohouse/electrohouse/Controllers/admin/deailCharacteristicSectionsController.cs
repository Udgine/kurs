﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using electrohouse.Models;
using electrohouse.Models.products;

namespace electrohouse.Controllers.admin
{
    public class deailCharacteristicSectionsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: deailCharacteristicSections
        public ActionResult Index(int? id)
        {
            if (id != null)
            {
                var characteristics = db.Items.FirstOrDefault(x => x.ID == id).characteristics.ToList();
                return View(characteristics);
            }
            return View(db.Characteristics.ToList());
        }

        // GET: deailCharacteristicSections/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            deailCharacteristicSection deailCharacteristicSection = db.Characteristics.Find(id);
            if (deailCharacteristicSection == null)
            {
                return HttpNotFound();
            }
            return View(deailCharacteristicSection);
        }

        // GET: deailCharacteristicSections/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: deailCharacteristicSections/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Caption")] deailCharacteristicSection deailCharacteristicSection)
        {
            if (ModelState.IsValid)
            {
                db.Characteristics.Add(deailCharacteristicSection);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(deailCharacteristicSection);
        }

        // GET: deailCharacteristicSections/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            deailCharacteristicSection deailCharacteristicSection = db.Characteristics.Find(id);
            if (deailCharacteristicSection == null)
            {
                return HttpNotFound();
            }
            return View(deailCharacteristicSection);
        }

        // POST: deailCharacteristicSections/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Caption")] deailCharacteristicSection deailCharacteristicSection)
        {
            if (ModelState.IsValid)
            {
                db.Entry(deailCharacteristicSection).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(deailCharacteristicSection);
        }

        // GET: deailCharacteristicSections/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            deailCharacteristicSection deailCharacteristicSection = db.Characteristics.Find(id);
            if (deailCharacteristicSection == null)
            {
                return HttpNotFound();
            }
            return View(deailCharacteristicSection);
        }

        // POST: deailCharacteristicSections/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            deailCharacteristicSection deailCharacteristicSection = db.Characteristics.Find(id);
            db.Characteristics.Remove(deailCharacteristicSection);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
