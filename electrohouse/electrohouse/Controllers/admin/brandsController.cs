﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using electrohouse.Models;
using electrohouse.Models.products;

namespace electrohouse.Controllers
{
    public class brandsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: brands
        public ActionResult Index()
        { 
            if (User.Identity.Name != "admin" && User.Identity.Name != "Alexi") { return Redirect(Url.Action("Index", "Home")); }
           
            return View(db.Brands.ToList());
        }

        // GET: brands/Details/5
        public ActionResult Details(int? id)
        {
            if (User.Identity.Name != "admin" && User.Identity.Name != "Alexi") { return Redirect(Url.Action("Index", "Home")); }

           


            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            brand brand = db.Brands.Find(id);
            if (brand == null)
            {
                return HttpNotFound();
            }
            return View(brand);
        }

        // GET: brands/Create
        public ActionResult Create()
        {
            if (User.Identity.Name != "admin" && User.Identity.Name != "Alexi") { return Redirect(Url.Action("Index", "Home")); }



            return View();
        }

        // POST: brands/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,name,category")] brand brand)
        {
            if (User.Identity.Name != "admin" && User.Identity.Name != "Alexi") { return Redirect(Url.Action("Index", "Home")); }



            if (ModelState.IsValid)
            {
                db.Brands.Add(brand);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(brand);
        }

        // GET: brands/Edit/5
        public ActionResult Edit(int? id)
        {
            if (User.Identity.Name != "admin" && User.Identity.Name != "Alexi") { return Redirect(Url.Action("Index", "Home")); }



            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            brand brand = db.Brands.Find(id);
            if (brand == null)
            {
                return HttpNotFound();
            }
            return View(brand);
        }

        // POST: brands/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,name,category")] brand brand)
        {
            if (User.Identity.Name != "admin" && User.Identity.Name != "Alexi") { return Redirect(Url.Action("Index", "Home")); }



            if (ModelState.IsValid)
            {
                db.Entry(brand).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(brand);
        }

        // GET: brands/Delete/5
        public ActionResult Delete(int? id)
        {
            if (User.Identity.Name != "admin" && User.Identity.Name != "Alexi") { return Redirect(Url.Action("Index", "Home")); }



            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            brand brand = db.Brands.Find(id);
            if (brand == null)
            {
                return HttpNotFound();
            }
            return View(brand);
        }

        // POST: brands/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (User.Identity.Name != "admin" && User.Identity.Name != "Alexi") { return Redirect(Url.Action("Index", "Home")); }



            brand brand = db.Brands.Find(id);
            db.Brands.Remove(brand);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        protected bool hasRight()
        {
            ApplicationDbContext db = new ApplicationDbContext();
            
            if (User.Identity.Name != "admin")
            {
                return false;
            }

            return true;
        }
    }
}
