﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using electrohouse.Models;

namespace electrohouse.Controllers.admin
{
    public class SController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: S
        public ActionResult Index()
        {
            if (User.Identity.Name != "admin" && User.Identity.Name != "Alexi") { return Redirect(Url.Action("Index", "Home")); }

            return View(db.Subscribers.ToList());
        }

        // GET: S/Details/5
        public ActionResult Details(int? id)
        {
            if (User.Identity.Name != "admin" && User.Identity.Name != "Alexi") { return Redirect(Url.Action("Index", "Home")); }

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Subscribe subscribe = db.Subscribers.Find(id);
            if (subscribe == null)
            {
                return HttpNotFound();
            }
            return View(subscribe);
        }

        // GET: S/Create
        public ActionResult Create()
        {
            if (User.Identity.Name != "admin" && User.Identity.Name != "Alexi") { return Redirect(Url.Action("Index", "Home")); }

            return View();
        }

        // POST: S/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,email")] Subscribe subscribe)
        {
            if (User.Identity.Name != "admin" && User.Identity.Name != "Alexi") { return Redirect(Url.Action("Index", "Home")); }

            if (ModelState.IsValid)
            {
                db.Subscribers.Add(subscribe);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(subscribe);
        }

        // GET: S/Edit/5
        public ActionResult Edit(int? id)
        {
            if (User.Identity.Name != "admin" && User.Identity.Name != "Alexi") { return Redirect(Url.Action("Index", "Home")); }

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Subscribe subscribe = db.Subscribers.Find(id);
            if (subscribe == null)
            {
                return HttpNotFound();
            }
            return View(subscribe);
        }

        // POST: S/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,email")] Subscribe subscribe)
        {
            if (User.Identity.Name != "admin" && User.Identity.Name != "Alexi") { return Redirect(Url.Action("Index", "Home")); }

            if (ModelState.IsValid)
            {
                db.Entry(subscribe).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(subscribe);
        }

        // GET: S/Delete/5
        public ActionResult Delete(int? id)
        {
            if (User.Identity.Name != "admin" && User.Identity.Name != "Alexi") { return Redirect(Url.Action("Index", "Home")); }

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Subscribe subscribe = db.Subscribers.Find(id);
            if (subscribe == null)
            {
                return HttpNotFound();
            }
            return View(subscribe);
        }

        // POST: S/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (User.Identity.Name != "admin" && User.Identity.Name != "Alexi") { return Redirect(Url.Action("Index", "Home")); }

            Subscribe subscribe = db.Subscribers.Find(id);
            db.Subscribers.Remove(subscribe);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
