﻿using electrohouse.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace electrohouse.Controllers
{
    public class SubscribeController : ApiController
    {

        private ApplicationDbContext db = new ApplicationDbContext();

        

        // POST: api/Subscribe
        public void Post([FromBody]dynamic values)
        {
            var httpContext = (HttpContextWrapper)Request.Properties["MS_HttpContext"];
            var email = httpContext.Request.Form.GetValues("email").FirstOrDefault();
            
            
            db.Subscribers.Add(new Subscribe(email));
            db.SaveChanges();
        }

      


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
