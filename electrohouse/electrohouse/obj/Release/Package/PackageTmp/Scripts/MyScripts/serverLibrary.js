﻿function subscribe() {

    var sub = document.getElementById("email").value;
    $.ajax({
        type: 'post',
        url: '/api/Subscribe/',
        dataType: 'json',
        data: {'email' : sub },
        
       
    }

    )
}

function askQuestion() {

    var q = document.getElementById("question").value;
    if (!(q === "")) {
        $.ajax({
            type: 'post',
            url: '/api/Questions/',
            dataType: 'json',
            data: { 'question': q },
            success: function () {
                document.getElementById("question").value = "";
            }
        }

        )
    }
    
}
function makeFeedback() {

    var sub = document.getElementById("email").value;
    var q = document.getElementById("question").value;
    $.ajax({
        type: 'post',
        url: '/api/Feedback/',
        dataType: 'json',
        data: { 'email': sub, 'question': q },

        success: function () {
            document.getElementById("question").value = "";
        }
    }

    )
}

function makeComment(id)
{
    var cmt = $('#comment').val();    
    var lalala = id;
    $.ajax({
        type: 'post',
        url: '/api/Comment/' + id,
        dataType: 'json',
        data: { 'comment': cmt },
        success: function (data)
        {
            var date = new Date(data.date);
           
           
           
            $('#comments').append(
                '<div class="review__heading" >' +
                '<span class="name" >' + data.userName + '</span >' +
                '<span class="date">' + formatDate(date) + '</span>' +
                '</div >' +
                '<p>' + data.comment + '</p>'
            );
           
        }
        }
    
    )
}

function formatDate(date) {

    var dd = date.getDate();
    if (dd < 10) dd = '0' + dd;

    var mm = date.getMonth() + 1;
    

    var yyyy = date.getFullYear();
    

    return mm + '/' + dd + '/' + yyyy;
}



