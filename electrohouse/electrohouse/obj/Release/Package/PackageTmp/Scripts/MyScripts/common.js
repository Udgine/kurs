function content() {
    var wind = $(window).width();

    if (wind < 481) {
        $('.catalog .catalog__wrapper .catalog__in').html('В наличии');
        $('.catalog .catalog__wrapper .catalog__in').css('margin-right', '0');
    }

    if (wind < 476) {
        $('.category .category__divider a.category__computer p').html('Компьютеры');
        $('.category .category__divider a.category__phone p').html('Смартфоны');
        $('.category .category__divider a.category__photo p').html('Фото');
    }

};

function cont() {
    var w = $(window).width();
    if (w < 380) {
        $('.item .nav li a.des').html('Опис.');
        $('.item .nav li a.cha').html('Хар.');
        $('.item .nav li a.rev').html('Отз.');
    }
};


$(window).on('load', function () {

    $(".loader_inner").fadeOut();
    $(".loader").delay(400).fadeOut("slow");


});

$(window).on('orientationChange', function () {




});

$(document).ready(function () {
    $('.js-index-slider').bxSlider({
        pagerCustom: '.js-index-slider__pager',
        controls: false,
        auto: true,
        pause: 5000,
        autoHover: true
    });

    $('.items-slider').bxSlider({
        pagerCustom: '.item-slider__pager',
        controls: false,
        auto: false
    });

    $('.items-slider-popular').bxSlider({
        pagerCustom: '.item-slider__pager-popular',
        controls: false,
        auto: false
    });

    $("#slider-range").slider({
        range: true,
        min: 0,
        max: 1000,
        values: [75, 300],
        step: 10,
        slide: function (event, ui) {
            $("#price").val("$" + ui.values[0] + " - $" + ui.values[1]);
        }
    });
    $("#price").val("$" + $("#slider-range").slider("values", 0) +
        " - $" + $("#slider-range").slider("values", 1));

    $('.profile__list').bxSlider({
        controls: true,
        auto: false,
        pager: false
    });

    $('button.toggle').click(function () {
        if ($('.toggle').hasClass('.active')) {
            $(this).removeClass('.active');
            $('.hide-menu').css('display', 'none');
            $('.topm').css('transform', 'rotate(0deg)');
            $('.topm').css('top', '9px');
            $('.middle').css('opacity', '1');
            $('.bottom').css('transform', 'rotate(0deg)');
            $('.bottom').css('bottom', '9px');
        }
        else {
            $('.topm').css('transform', 'rotate(45deg)');
            $('.hide-menu').css('display', 'block');
            $('.topm').css('top', '17px');
            $('.middle').css('opacity', '0');
            $('.bottom').css('transform', 'rotate(-45deg)');
            $('.bottom').css('bottom', '15px');
            $(this).addClass('.active');
        }
    });

    function loginStr() {
        var str = $('header .top .menu .account a').html();
        var newStr;
        if (str.length > 14) {
            newStr = str.substring(0, 10) + '...';
            $('header .top .menu .account a').html(newStr);
        }
    };

    loginStr();

    function locationInHideMenu() {
        var location = window.location.href;
        var cur_url = '/' + location.split('/').pop();
        $('.hide-menu .hide-menu__list li').each(function () {
            var link = $(this).find('a').attr('href');
            if (cur_url == link)
                $(this).find('a').addClass('active-item');
        });
    };

    $('.item .item__photo img').click(function () {
        var src = $(this).attr('src');
        $('article .img-viewer .img-viewer__content img').attr('src', src);
        $('article .img-viewer').toggle('slow');
    });

    $('.item .item__photos img').click(function () {
        var src = $(this).attr('src');
        $('article .img-viewer .img-viewer__content img').attr('src', src);
        $('article .img-viewer').toggle('slow');

    });

    $('article .img-viewer .img-viewer__content i').click(function () {
        $('article .img-viewer').toggle('slow');
    });

    function locationInMenu() {
        var location = window.location.href;
        var cur_url = '/' + location.split('/').pop();
        $('.menu-main .navigation .menu-items li').each(function () {
            var link = $(this).find('a').attr('href');
            if (cur_url == link)
                $(this).find('a').addClass('active-item');
        });
    };

    function locationInFooterMenu() {
        var location = window.location.href;
        var cur_url = '/' + location.split('/').pop();
        $('footer .footer .footer__wrapper .footer__main-menu li').each(function () {
            var link = $(this).find('a').attr('href');
            if (cur_url == link)
                $(this).find('a').addClass('active-item');
        });
    };

    locationInHideMenu();
    locationInMenu();
    locationInFooterMenu();




    cont();

   

    $('.catalog .catalog__options .catalog__sorting a').click(function () {
        $('.catalog__list').toggle('slow');
    });


    $('.questions .questions__list li').click(function () {
        if ($(this).find('p').hasClass('active')) {
            $(this).find('p').toggle('slow');
            $(this).find('p').removeClass('active');
        } else {
            $(this).parent().find('li').each(function () {
                $(this).find('p').hide('slow');
                $(this).find('p').removeClass('active');
            });
            $(this).find('p').toggle('slow');
            $(this).find('p').addClass('active');
        }





    });

    



    $('.catalog .catalog__options .catalog__filter').click(function () {
        if ($('.filter__wrap').hasClass('.active')) {
            $('.filter').css('z-index', '0');
            $('.filter__wrap').removeClass('.active');
            $('.filter__wrap').css('opacity', '0');
        } else {
            $('.filter').css('z-index', '330');
            $('.filter__wrap').addClass('.active');
            $('.filter__wrap').css('opacity', '1')
        }


    });

    $('.filter .filter__wrap form ul .filter__buttons a').click(function () {
        $('.filter').css('z-index', '0');
        $('.filter__wrap').removeClass('.active');
        $('.filter__wrap').css('opacity', '0');
    });



    function activeItem() {
        var location = window.location.href;
        var cur_url = '/' + location.split('/').pop();
        var reg = cur_url.indexOf('/catalog.html')
        if (reg !== -1) {
            $('.hide-menu .hide-menu__list li').each(function () {
                var m = $(this).find('a').attr('href');
                if (m == '/category.html') {
                    $(this).find('a').addClass('active-item');
                }
            });
        }

        if (reg !== -1) {
            $('.menu-main .navigation .menu-items li').each(function () {
                var m = $(this).find('a').attr('href');
                if (m == '/category.html') {
                    $(this).find('a').addClass('active-item');
                }
            });
        }

        if (reg !== -1) {
            $('footer .footer .footer__wrapper .footer__main-menu li').each(function () {
                var m = $(this).find('a').attr('href');
                if (m == '/category.html') {
                    $(this).find('a').addClass('active-item');
                }
            });
        }
    };

    $("body > div:last-child").hide();


    activeItem();


    content();


});
$(window).resize(function () {
    content();
    cont();
});