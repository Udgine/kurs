﻿namespace electrohouse.Migrations
{
    using electrohouse.Models;
    using electrohouse.Models.products;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<electrohouse.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "electrohouse.Models.ApplicationDbContext";
        }

        protected override void Seed(electrohouse.Models.ApplicationDbContext context)
        {
            var brnd = new List<brand>
            {
                new brand("Samsung"),
                new brand("Apple"),
                new brand("Xiomi"),
                new brand("Nokia")
            };
            brnd.ForEach(e => context.Brands.AddOrUpdate(p => p.ID, e));


            var itm = new List<item>
            {
                new item("phone1", 501, Category.phone, brnd[0], true, false, false, "/Content/images/items/1.png", 1, 1),
                new item("phone2", 502, Category.phone, brnd[1], false, true, false, "/Content/images/items/1.png", 1, 1),
                new item("phone3", 503, Category.phone, brnd[2], false, false, true, "/Content/images/items/1.png", 1, 1),
                new item("phone4", 504, Category.phone, brnd[3], false, true, false, "/Content/images/items/1.png", 1, 1),
                new item("phone5", 505, Category.phone, brnd[0], true, false, false, "/Content/images/items/1.png", 1, 1),
                new item("phone6", 506, Category.phone, brnd[1], false, true, false, "/Content/images/items/1.png", 1, 1)
            };
            itm.ForEach(e => context.Items.AddOrUpdate(p => p.ID, e));


            var qstn = new List<Question>
            {
                new Question("Осуществляете ли Вы доставку до квартиры и подъем на этаж?","Пожалуйста, обратите внимание, что адресная доставка осуществляется до входа в здание. Крупногабаритные товары , а так же товары из раздела «Бытовая техника» доставляются до квартиры. Стоимость ручного заноса в квартиру зависит от: габаритов техники, сложности подъема и наличия лифта (куда товар помещается в упаковке)."),
                new Question("Сколько дней товар находится в пункте выдачи?","В отделении курьерской службы «Новая Почта» заказ будет находиться в течении 5 дней. По истечению данного срока, товар возвращается отправителю."),
                new Question("Требуется ли дополнительная оплата за перевод средств при наложенном платеже в отделении Новой Почты?","Дополнительная оплата за пересылку средств при наложенном платеже с вашей стороны не требуется. Стоимость заказа, указанная при подтверждении заказа, является окончательной."),
                new Question("Есть ли возможность отказаться от части заказа в пункте выдачи?","К сожалению, возможности отказаться от части заказа нет. Вы можете отказаться от неактуального заказа полностью и оформить новую заявку."),
                new Question("Возможно ли оплатить заказ картой при получении?","В данный момент, к сожалению, отсутствует возможность оплатить заказ платежной картой при получении (с помощью POS-терминала). Однако, Вы можете оплатить заказ онлайн картой Visa и MasterCard или оформить заказ с указанием безналичной формы оплаты. Уточняем, что услуга оплаты картой доступна только при оформлении заказа на нашем сайте. Для этого, при оформлении заказа у нас сайте, Вам необходимо выбрать соответствующий вид оплаты. После оформления заявки на сайте — наш менеджер свяжется с Вами для уточнения информации относительно доставки Вашего заказа.")

            };

            qstn.ForEach(e => context.Questions.AddOrUpdate(p => p.ID, e));

            context.SaveChanges();
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
