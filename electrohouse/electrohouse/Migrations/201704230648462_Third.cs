namespace electrohouse.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Third : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.items", "isNew", c => c.Boolean(nullable: false));
            AlterColumn("dbo.items", "isStock", c => c.Boolean(nullable: false));
            AlterColumn("dbo.items", "isPopular", c => c.Boolean(nullable: false));
            AlterColumn("dbo.Feedbacks", "isSend", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Feedbacks", "isSend", c => c.Int(nullable: false));
            AlterColumn("dbo.items", "isPopular", c => c.Int(nullable: false));
            AlterColumn("dbo.items", "isStock", c => c.Int(nullable: false));
            AlterColumn("dbo.items", "isNew", c => c.Int(nullable: false));
        }
    }
}
