namespace electrohouse.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Seven : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.items", "alterPath1", c => c.String());
            AddColumn("dbo.items", "alterPath2", c => c.String());
            AddColumn("dbo.items", "alterPath3", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.items", "alterPath3");
            DropColumn("dbo.items", "alterPath2");
            DropColumn("dbo.items", "alterPath1");
        }
    }
}
