namespace electrohouse.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Fifth : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.items", "priceOld", c => c.Double(nullable: false));
            AddColumn("dbo.items", "description", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.items", "description");
            DropColumn("dbo.items", "priceOld");
        }
    }
}
