namespace electrohouse.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class firstMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.brands",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        name = c.String(),
                        category = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.items",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        name = c.String(),
                        price = c.Double(nullable: false),
                        Category = c.Int(nullable: false),
                        isNew = c.Boolean(nullable: false),
                        isStock = c.Boolean(nullable: false),
                        isPopular = c.Boolean(nullable: false),
                        path = c.String(),
                        rating = c.Int(nullable: false),
                        likes = c.Int(nullable: false),
                        brand_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.brands", t => t.brand_ID)
                .Index(t => t.brand_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.items", "brand_ID", "dbo.brands");
            DropIndex("dbo.items", new[] { "brand_ID" });
            DropTable("dbo.items");
            DropTable("dbo.brands");
        }
    }
}
