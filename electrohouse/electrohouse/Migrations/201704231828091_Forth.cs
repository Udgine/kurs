namespace electrohouse.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Forth : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.deailCharacteristicSections",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Caption = c.String(),
                        item_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.items", t => t.item_ID)
                .Index(t => t.item_ID);
            
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        userName = c.String(),
                        comment = c.String(),
                        date = c.DateTime(nullable: false),
                        item_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.items", t => t.item_ID)
                .Index(t => t.item_ID);
            
            AddColumn("dbo.items", "available", c => c.Boolean(nullable: false));
            AddColumn("dbo.items", "guarantee", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Comments", "item_ID", "dbo.items");
            DropForeignKey("dbo.deailCharacteristicSections", "item_ID", "dbo.items");
            DropIndex("dbo.Comments", new[] { "item_ID" });
            DropIndex("dbo.deailCharacteristicSections", new[] { "item_ID" });
            DropColumn("dbo.items", "guarantee");
            DropColumn("dbo.items", "available");
            DropTable("dbo.Comments");
            DropTable("dbo.deailCharacteristicSections");
        }
    }
}
