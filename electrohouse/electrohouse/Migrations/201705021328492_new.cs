namespace electrohouse.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _new : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "nameOfCustomer", c => c.String());
            AddColumn("dbo.Orders", "email", c => c.String());
            AddColumn("dbo.Orders", "phone", c => c.String());
            AddColumn("dbo.Orders", "town", c => c.String());
            AddColumn("dbo.Orders", "deliveryType", c => c.String());
            AddColumn("dbo.Orders", "paymentType", c => c.String());
            AddColumn("dbo.AspNetUsers", "town", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "town");
            DropColumn("dbo.Orders", "paymentType");
            DropColumn("dbo.Orders", "deliveryType");
            DropColumn("dbo.Orders", "town");
            DropColumn("dbo.Orders", "phone");
            DropColumn("dbo.Orders", "email");
            DropColumn("dbo.Orders", "nameOfCustomer");
        }
    }
}
