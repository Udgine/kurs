namespace electrohouse.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Second : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Feedbacks",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        email = c.String(),
                        question = c.String(),
                        isSend = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        userId = c.String(),
                        state = c.Int(nullable: false),
                        RegistrationDate = c.DateTime(nullable: false),
                        totalItems = c.Int(nullable: false),
                        totalPrice = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Questions",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        question = c.String(),
                        answer = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Subscribes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        email = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            AlterColumn("dbo.items", "isNew", c => c.Int(nullable: false));
            AlterColumn("dbo.items", "isStock", c => c.Int(nullable: false));
            AlterColumn("dbo.items", "isPopular", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.items", "isPopular", c => c.Boolean(nullable: false));
            AlterColumn("dbo.items", "isStock", c => c.Boolean(nullable: false));
            AlterColumn("dbo.items", "isNew", c => c.Boolean(nullable: false));
            DropTable("dbo.Subscribes");
            DropTable("dbo.Questions");
            DropTable("dbo.Orders");
            DropTable("dbo.Feedbacks");
        }
    }
}
