namespace electrohouse.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _1 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Orders", "nameOfCustomer");
            DropColumn("dbo.Orders", "email");
            DropColumn("dbo.Orders", "phone");
            DropColumn("dbo.Orders", "town");
            DropColumn("dbo.Orders", "deliveryType");
            DropColumn("dbo.Orders", "paymentType");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Orders", "paymentType", c => c.String());
            AddColumn("dbo.Orders", "deliveryType", c => c.String());
            AddColumn("dbo.Orders", "town", c => c.String());
            AddColumn("dbo.Orders", "phone", c => c.String());
            AddColumn("dbo.Orders", "email", c => c.String());
            AddColumn("dbo.Orders", "nameOfCustomer", c => c.String());
        }
    }
}
