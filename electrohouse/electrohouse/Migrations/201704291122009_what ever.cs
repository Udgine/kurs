namespace electrohouse.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class whatever : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.brands", "category");
        }
        
        public override void Down()
        {
            AddColumn("dbo.brands", "category", c => c.Int(nullable: false));
        }
    }
}
