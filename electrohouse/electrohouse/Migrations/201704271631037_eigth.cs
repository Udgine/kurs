namespace electrohouse.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class eigth : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CharacteristicStrings",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        characteristic = c.String(),
                        isShort = c.Boolean(nullable: false),
                        deailCharacteristicSection_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.deailCharacteristicSections", t => t.deailCharacteristicSection_ID)
                .Index(t => t.deailCharacteristicSection_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CharacteristicStrings", "deailCharacteristicSection_ID", "dbo.deailCharacteristicSections");
            DropIndex("dbo.CharacteristicStrings", new[] { "deailCharacteristicSection_ID" });
            DropTable("dbo.CharacteristicStrings");
        }
    }
}
