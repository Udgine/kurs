namespace electrohouse.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class six : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.items", "CategoryName", c => c.String());
            DropColumn("dbo.items", "rating");
        }
        
        public override void Down()
        {
            AddColumn("dbo.items", "rating", c => c.Int(nullable: false));
            DropColumn("dbo.items", "CategoryName");
        }
    }
}
